package VendingMachine;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class VendingMachineTest {
	VendingMachine vm;
    String newlineChar = System.getProperty("line.separator");

	@Before
	public void setUp() throws Exception {
		vm = new VendingMachine();
        vm.powerUpVendingMechine();
	}

	@Test
	public void testCalculateChange() {
		assertEquals(0.7,vm.calculateChange(3.0, "TC FC OE TE n"),0.1);
	}
	
	@Test
	public void testCalculateChange2() {
		PrintStream oldout = System.out;
		
		ByteArrayOutputStream newout = new ByteArrayOutputStream();
		System.setOut(new PrintStream(newout));

		
		vm.calculateChange(3.0, "CANCEL");
		
		String reply = newout.toString();
				
		assertTrue(reply.contains("You have pressed CANCEL"));
		
		System.setOut(oldout);

	}
	
	@Test
	public void testCalculateChange3() {
		assertEquals(-3.0,vm.calculateChange(3.0,""), 0.1);
	}
	
	@Test
	public void testCalculateChange4() {
		assertEquals(0.0,vm.calculateChange(3.0,"TE OE"), 0.1);
	}
	
	@Test
	public void testCaptureMoney() {
		PrintStream oldout = System.out;
		
		ByteArrayOutputStream newout = new ByteArrayOutputStream();
		System.setOut(new PrintStream(newout));
		
		ByteArrayInputStream input = new ByteArrayInputStream(("TE"+newlineChar+" TE"+ newlineChar).getBytes());
		System.setIn(input);
		
		vm.captureMoney("COLA", 3.0);
		
		String reply = newout.toString();
				
		assertTrue(reply.contains("Your change is: 1.0 EURO"));
		
		System.setOut(oldout);
		System.setIn(System.in);
	}
	

}
